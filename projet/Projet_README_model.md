# Groupe
- Yang Zhuangzhuang
- Yuan Rong

# Lien du jeu de données
https://www.data.gouv.fr/fr/datasets/emissions-de-co2-et-de-polluants-des-vehicules-commercialises-en-france/

# Questions proposées
- Quelle substance chimique la voiture émet-elle le plus?
- Quel carburant la voiture utilise-t-elle pour minimiser la pollution?
- Comment choisir un véhicule plus économique et respectueux de l'environnement?


# Question retenue
Quels sont les principaux facteurs affectant la quantité de pollution émise par les véhicules?
# Remarques éventuelles
[...]
